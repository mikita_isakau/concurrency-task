package by.epam.training.caches;

import org.junit.Assert;
import org.junit.Test;
import java.util.Arrays;
import java.util.List;

/**
 * Package: by.epam.training.caches
 * Project name: Collection-task
 * Created by Mikita Isakau
 * Creation date: 01.12.2016
 * Test-case for LRUCache class
 */
public class LRUCacheTest {

    /**
     * Test get from empty cache.
     */
    @Test
    public void testGetFromEmptyCache(){
        LRUCache<String> testCache = new LRUCache<>();
        Assert.assertEquals(null, testCache.get(0));
    }

    /**
     * Test set to cache with one element.
     */
    @Test
    public void testSetToCacheWithOneElement(){
        LRUCache<String> testCache = new LRUCache<>();
        testCache.set("Hello");
        Assert.assertEquals("Hello", testCache.get(0));
    }

    /**
     * Test get from cache.
     */
    @Test
    public void testGetFromCache(){
        LRUCache<String> testCache = new LRUCache<>();
        testCache.set("Hello");
        testCache.set("World");
        Assert.assertEquals(null, testCache.get(Integer.MAX_VALUE));
    }

    /**
     * Test set to cache with index.
     */
    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testSetToCacheWithIndex(){
        LRUCache<String> testCache = new LRUCache<>();
        testCache.set("Hello");
        testCache.set(Integer.MAX_VALUE,"World");
    }

    /**
     * Test get all from cache.
     */
    @Test
    public void testGetAll(){
        LRUCache<String> testCache = new LRUCache<>();
        testCache.set("Hello");
        testCache.set("World");
        Assert.assertEquals(Arrays.asList(new String[]{"Hello","World"}),
                testCache.getAll());
    }

    /**
     * Test capacity with empty cache.
     */
    @Test
    public void testCapacityWithEmptyCache(){
        LRUCache<String> testCache = new LRUCache<>(0);
        Assert.assertEquals(0, testCache.capacity());
    }

    /**
     * Test capacity with default cache size.
     */
    @Test
    public void testCapacityWithDefaultCacheSize(){
        LRUCache<String> testCache = new LRUCache<>();
        Assert.assertEquals(10, testCache.capacity());
    }

    /**
     * Test capacity with custom cache size.
     */
    @Test
    public void testCapacityWithCustomCacheSize(){
        LRUCache<String> testCache = new LRUCache<>(256);
        Assert.assertEquals(256, testCache.capacity());
    }

    /**
     * Test empty cache size.
     */
    @Test
    public void testEmptyCacheSize(){
        LRUCache<String> testCache = new LRUCache<>();
        Assert.assertEquals(0, testCache.size());
    }

    /**
     * Test cache size.
     */
    @Test
    public void testCacheSize(){
        LRUCache<String> testCache = new LRUCache<>();
        testCache.set("Hello");
        testCache.set("World");
        Assert.assertEquals(2, testCache.size());
    }

    /**
     * Test set last element of cache.
     */
    @Test
    public void testSetLastElementOfCache(){
        LRUCache<String> testCache = new LRUCache<>(20);
        testCache.set(testCache.capacity() - 1,"World");
        Assert.assertEquals(testCache.capacity(), 20);
    }

    /**
     * Test set last element of cache with get all.
     */
    @Test
    public void testSetLastElementOfCacheWithGetAll(){
        LRUCache<String> testCache = new LRUCache<>(20);
        testCache.set(testCache.capacity() - 1,"World");
        Assert.assertEquals(Arrays.asList(new String[]{"World"}),
                testCache.getAll());
    }

    /**
     * Test set last element of cache with get all when last element was override.
     */
    @Test
    public void testSetLastElementOfCacheWithGetAllWhenOverrideLastElement(){
        LRUCache<String> testCache = new LRUCache<>(20);
        testCache.set(testCache.capacity() - 1,"HELLO");
        testCache.set(testCache.capacity() - 1,"World");
        List<String> requiredResult = Arrays.asList(new String[]{"World"});
        Assert.assertEquals(requiredResult, testCache.getAll());
    }

}

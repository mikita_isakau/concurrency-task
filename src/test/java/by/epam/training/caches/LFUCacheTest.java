package by.epam.training.caches;

import org.junit.Assert;
import org.junit.Test;

/**
 * Package: by.epam.training.caches
 * Project name: Collection-task
 * Created by Mikita Isakau
 * Creation date: 01.12.2016
 */
public class LFUCacheTest {

    /**
     * Test empty cache.
     */
    @Test
    public void testEmptyCache(){
        LFUCache<String> lfu = new LFUCache<>();
        Assert.assertEquals(null, lfu.getElement(""));
    }

    /**
     * Test empty size cache.
     */
    @Test
    public void testEmptySizeCache(){
        LFUCache<String> lfu = new LFUCache<>();
        Assert.assertEquals(0, lfu.size());
    }

    /**
     * Test capacity when cache is empty.
     */
    @Test
    public void testCapacityWhenCacheIsEmpty(){
        LFUCache<String> lfu = new LFUCache<>(100, 1f);
        Assert.assertEquals(100, lfu.capacity());
    }

    /**
     * Test capacity when cache has some elements.
     */
    @Test
    public void testCapacityWhenCacheHasSomeElements(){
        LFUCache<String> lfu = new LFUCache<>(100, 1f);
        lfu.setElement("Hello");
        lfu.setElement("Hi");
        lfu.setElement("My");
        lfu.setElement("Name");
        lfu.setElement("Is");
        lfu.setElement("Borat");
        lfu.setElement("Sagdiev");
        Assert.assertEquals(100, lfu.capacity());
    }

    /**
     * Test cache with one element.
     */
    @Test
    public void testCacheWithOneElement(){
        LFUCache<String> lfu = new LFUCache<>();
        lfu.setElement("test");
        Assert.assertEquals("test", lfu.getElement("test"));
    }

    /**
     * Test cache before load factory.
     */
    @Test
    public void testCacheBeforeLoadFactory(){
        LFUCache<String> lfu = new LFUCache<>(10,0.75f);
        lfu.setElement("Hello");
        lfu.setElement("Hi");
        lfu.setElement("My");
        lfu.setElement("Name");
        lfu.setElement("Is");
        lfu.setElement("Borat");
        lfu.setElement("Sagdiev");
        Assert.assertEquals(7, lfu.size());
    }

    /**
     * Test cache after load factory.
     */
    @Test
    public void testCacheAfterLoadFactory(){
        LFUCache<String> lfu = new LFUCache<>(10,0.75f);
        lfu.setElement("Hello");
        lfu.setElement("Hi");
        lfu.setElement("My");
        lfu.setElement("Name");
        lfu.setElement("Is");
        lfu.setElement("Borat");
        lfu.setElement("Sagdiev");
        lfu.setElement("I like it!");
        Assert.assertEquals(1, lfu.size());
    }

    /**
     * Test cache with remove element.
     */
    @Test
    public void testCacheWithRemoveElement(){
        LFUCache<String> lfu = new LFUCache<>(10,0.75f);
        lfu.setElement("Borat");
        lfu.setElement("Sagdiev");
        lfu.remove("Sagdiev");
        Assert.assertEquals(null, lfu.getElement("Sagdiev"));
    }


}

package by.epam.training.caches;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Package: by.epam.training.caches
 * Project name: Collection-task
 * Created by Mikita Isakau
 * Creation date: 01.12.2016
 *
 * @param <E> the type parameter
 */
public class LFUCache<E> {
    /**
     * The constant DEFAULT_LOAD_FACTORY.
     */
    private static final float DEFAULT_LOAD_FACTORY = 0.75f;
    /**
     * The constant DEFAULT_CAPACITY.
     */
    private static final int DEFAULT_CAPACITY = 10;

    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private List<LinkedList<E>> storage;
    private float loadFactory;
    private int capacity;
    private int size;

    /**
     * Instantiates a new Lfu cache.
     */
    public LFUCache() {
        this(DEFAULT_CAPACITY, DEFAULT_LOAD_FACTORY);
    }

    /**
     * Instantiates a new Lfu cache.
     *
     * @param capacity    the capacity
     * @param loadFactory the load factory
     */
    public LFUCache(final int capacity, final float loadFactory) {
        storage = new ArrayList<>(capacity + 1);
        this.capacity = capacity;
        this.loadFactory = loadFactory;
        this.size = 0;

        for (int i = 0; i < capacity + 1; i++) {
            storage.add(new LinkedList<>());
        }
    }

    /**
     * Get element e.
     *
     * @param e the e
     * @return the e
     */
    public E getElement(final E e) {
        E element = null;
        readWriteLock.readLock().lock();
        int key = 0;
        for (List<E> eList : storage) {
            key++;
            if (eList.contains(e)) {
                int index = eList.indexOf(e);
                element = eList.get(index);
                eList.remove(element);
                break;
            }
        }
        if (Objects.isNull(element)) {
            return element;
        }
        if (key > capacity) {
            key = capacity;
        }
        storage.get(key).add(element);
        readWriteLock.readLock().unlock();
        return element;
    }

    /**
     * Set element boolean.
     *
     * @param e the e
     * @return the boolean
     */
    public boolean setElement(final E e) {
        readWriteLock.writeLock().lock();
        try {
            clean();
            if (storage.get(0).add(e)) {
                size++;
                return true;
            }
            return false;
        } finally {
            readWriteLock.writeLock().unlock();
        }

    }

    /**
     * Remove element e.
     *
     * @param e the e
     * @return the e
     */
    public E remove(final E e) {
        E el = null;
        readWriteLock.writeLock().lock();
        try {
            for (List<E> eList : storage) {
                if (!eList.contains(e)) {
                   continue;
                }
                int index = eList.indexOf(e);
                el = eList.remove(index);
                size--;
            }
        } finally {
            readWriteLock.writeLock().unlock();
            return el;
        }
    }

    /**
     * Size of cache.
     *
     * @return the int
     */
    public int size() {
        readWriteLock.readLock().lock();
        int size = this.size;
        readWriteLock.readLock().unlock();
        return size;
    }

    /**
     * Capacity of cache.
     *
     * @return the int
     */
    public int capacity() {
        readWriteLock.readLock().lock();
        int capacity = this.capacity;
        readWriteLock.readLock().unlock();
        return capacity;
    }

    /**
     * Get all elements with frequency position list.
     *
     * @return the list
     */
    public List<E> getAllElementsWithFrequencyPosition() {
        readWriteLock.readLock().lock();
        List<E> result = new ArrayList<>();
        int i = 0;
        for (List<E> eList : storage) {
            for (E e : eList) {
                System.out.print(e + " ");
                result.add(e);
            }
            System.out.println("(" + i + ")");
            i++;
        }
        readWriteLock.readLock().unlock();
        return result;
    }

    private void clean() {
        int countToDelete = (int) Math.floor(loadFactory * capacity);
        if ((size < countToDelete)) {
           return;
        }
        int i = 0;
        for (List<E> eList : storage) {
            Iterator<E> element = eList.iterator();
            while (element.hasNext()) {
                if (i > countToDelete) {
                    continue;
                }
                element.next();
                element.remove();
                i++;
            }
        }
        size -= countToDelete;
    }
}

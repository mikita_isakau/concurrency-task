package by.epam.training.caches;

import java.util.*;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Package: by.epam.training
 * Project name: Collection-task
 * Created by Mikita Isakau
 * Creation date: 30.11.2016
 *
 * @param <E> the type parameter
 */
public class LRUCache<E> extends LinkedHashMap<Integer, E> {

    private static final float LOAD_FACTOR = 0.75f;

    private int cacheSize;
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private int cachePoint;

    /**
     * Instantiates a new Lru cache.
     */
    public LRUCache() {
        this(10);
    }

    /**
     * Instantiates a new Lru cache.
     *
     * @param capacity the capacity
     */
    public LRUCache(final int capacity) {
        super(capacity, LOAD_FACTOR, true);
        this.cacheSize = capacity;
        this.cachePoint = 0;
    }

    protected boolean removeEldestEntry(final Map.Entry<Integer, E> eldest) {
        return size() > cacheSize;
    }

    /**
     * Get element e.
     *
     * @param index the index
     * @return the e
     */
    public E get(final int index) {
        readWriteLock.readLock().lock();
        E e = super.get(index);
        readWriteLock.readLock().unlock();
        return e;
    }

    /**
     * Set element e.
     *
     * @param e the e
     * @return the e
     */
    public E set(final E e) {
        readWriteLock.writeLock().lock();
        try {
            if (cachePoint >= cacheSize) {
                cachePoint = cacheSize - 1;
            }
            return put(cachePoint++, e);
        } finally {
            readWriteLock.writeLock().unlock();
        }

    }

    /**
     * Set e.
     *
     * @param index the index
     * @param e     the e
     * @return the e
     */
    public E set(final int index, final E e) {
        readWriteLock.writeLock().lock();
        try {
            if (index >= cacheSize || index < 0) {
                throw new ArrayIndexOutOfBoundsException(index);
            }
            return put(index, e);
        } finally {
            readWriteLock.writeLock().unlock();
        }

    }

    /**
     * Remove e.
     *
     * @param index the index
     * @return the e
     */
    public E remove(final int index) {
        readWriteLock.writeLock().lock();
        try {
            return super.remove(index);
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    /**
     * Capacity size.
     *
     * @return the int
     */
    public int capacity() {
        readWriteLock.readLock().lock();
        int cap = cacheSize;
        readWriteLock.readLock().unlock();
        return cap;
    }

    /**
     * Gets all elements.
     *
     * @return the all
     */
    public List<E> getAll() {
        readWriteLock.readLock().lock();
        List<E> list = new ArrayList<>(values());
        readWriteLock.readLock().unlock();
        return list;
    }
}
